'use strict';
/***
Metronic AngularJS App Main Script
***/

/* Metronic App */
var MetronicApp = angular.module("MetronicApp", [
    "ui.router", 
    "ui.bootstrap", 
    "oc.lazyLoad",  
    "ngSanitize"
])
.constant('appSettings', {
  db: 'https://couchdb-3495ce.smileupps.com/startupbundle'
    }); 

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MetronicApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

/********************************************
 BEGIN: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/
/**
`$controller` will no longer look for controllers on `window`.
The old behavior of looking on `window` for controllers was originally intended
for use in examples, demos, and toy apps. We found that allowing global controller
functions encouraged poor practices, so we resolved to disable this behavior by
default.

To migrate, register your controllers with modules rather than exposing them
as globals:

Before:

```javascript
function MyController() {
  // ...
}
```

After:

```javascript
angular.module('myApp', []).controller('MyController', [function() {
  // ...
}]);

Although it's not recommended, you can re-enable the old behavior like this:

```javascript
angular.module('myModule').config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);
**/

//AngularJS v1.3.x workaround for old style controller declarition in HTML
MetronicApp.config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

/* Setup global settings */
MetronicApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: Metronic.getAssetsPath() + 'admin/layout/img/',
        layoutCssPath: Metronic.getAssetsPath() + 'admin/layout/css/'
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* Setup App Main Controller */
MetronicApp.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function() {
        Metronic.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
    });
}]);
//anchor table
// MetronicApp.controller('AgentController', ['$scope', '$rootScope', function($scope, $rootScope) {
//     $scope.$on('$viewContentLoaded', function() {
//         TableEditable.init();
//         //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
//     });
// }]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
MetronicApp.controller('HeaderController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('SidebarController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Quick Sidebar */
MetronicApp.controller('QuickSidebarController', ['$scope', function($scope) {    
    $scope.$on('$includeContentLoaded', function() {
        setTimeout(function(){
            QuickSidebar.init(); // init quick sidebar        
        }, 2000)
    });
}]);

/* Setup Layout Part - Theme Panel */
MetronicApp.controller('ThemePanelController', ['$scope', function($scope) {    
    $scope.$on('$includeContentLoaded', function() {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
MetronicApp.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
MetronicApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/dashboard.html");  
    $stateProvider
        
        // Dashboard
        .state('dashboard', {
            url: "/dashboard.html",
            templateUrl: "views/dashboard.html",            
            data: {pageTitle: 'Admin Dashboard Template'},
            controller: "DashboardController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'styles/assets/global/plugins/morris/morris.css',
                            'styles/assets/admin/pages/css/tasks.css',
                            
                            'styles/assets/global/plugins/morris/morris.min.js',
                            'styles/assets/global/plugins/morris/raphael-min.js',
                            'styles/assets/global/plugins/jquery.sparkline.min.js',

                            'styles/assets/admin/pages/scripts/index3.js',
                            'styles/assets/admin/pages/scripts/tasks.js',

                             'js/controllers/DashboardController.js'
                        ] 
                    });
                }]
            }
        })
        // Add Log
         .state('addlog', {
            url: "/addlog.html",
            templateUrl: "views/addlog.html",            
            data: {pageTitle: 'Agent | Add Log'},
            controller: "AddlogController",
              resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            'styles/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css',
                            'styles/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                            'styles/assets/global/plugins/typeahead/typeahead.css',

                            'styles/assets/global/plugins/fuelux/js/spinner.min.js',
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            'styles/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            'styles/assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
                            'styles/assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                            'styles/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js',
                            'styles/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                            'styles/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
                            'styles/assets/global/plugins/typeahead/handlebars.min.js',
                            'styles/assets/global/plugins/typeahead/typeahead.bundle.min.js',
                            'styles/assets/admin/pages/scripts/components-form-tools.js',
                            'styles/assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            'styles/assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                            'js/controllers/AddlogController.js'
                        ] 
                    }]);
                }] 
            }
       
        })
        
        // All invoices
         .state('invoices', {
            url: "/invoices",
            templateUrl: "views/invoices.html",            
            data: {pageTitle: 'Agent | All Invoices'},
            controller: "InvoiceController",
              resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'styles/assets/global/plugins/select2/select2.css',                             
                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css', 
                            'styles/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css',
                            'styles/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css',

                            'styles/assets/global/plugins/select2/select2.min.js',
                            'styles/assets/global/plugins/datatables/all.min.js',
                            'js/scripts/table-advanced.js',
                            
                            'js/controllers/InvoiceController.js'
                        ] 
                    }]);
                }] 
            }
       
        })

        // View customers
         .state('viewCustomers', {
            url: "/viewCustomers",
            templateUrl: "views/viewCustomers.html",            
            data: {pageTitle: 'Agent | View Customers'},
            controller: "ViewCustomersController",
              resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'styles/assets/global/plugins/select2/select2.css',                             
                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css', 
                            'styles/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css',
                            'styles/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css',

                            'styles/assets/global/plugins/select2/select2.min.js',
                            'styles/assets/global/plugins/datatables/all.min.js',
                            'js/scripts/table-advanced.js',

                            'js/controllers/ViewCustomersController.js'
                        ] 
                    }]);
                }] 
            }
       
        })

        // View customer by Id
         .state('customer', {
            url: "/customer",
            params:{
                custId:{},
            },
            templateUrl: "views/customer.html",            
            data: {pageTitle: 'Agent | View Customers'},
            controller: "CustomerController",
              resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'styles/assets/global/plugins/select2/select2.css',                             
                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css', 
                            'styles/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css',
                            'styles/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css',

                            'styles/assets/global/plugins/select2/select2.min.js',
                            'styles/assets/global/plugins/datatables/all.min.js',
                            'js/scripts/table-advanced.js',

                            'js/controllers/CustomerController.js'
                        ] 
                    }]);
                }] 
            }
       
        })

        .state('product', {
            url: "/products",
            templateUrl: "views/product.html",            
            data: {pageTitle: 'Admin|Products'},
            controller: "ProductController",
              resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            'styles/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css',
                            'styles/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                            'styles/assets/global/plugins/typeahead/typeahead.css',

                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',
                            'styles/assets/global/plugins/select2/select2.css',

                            'styles/assets/global/plugins/fuelux/js/spinner.min.js',
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            'styles/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            'styles/assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
                            'styles/assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                            'styles/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js',
                            'styles/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                            'styles/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
                            'styles/assets/global/plugins/typeahead/handlebars.min.js',
                            'styles/assets/global/plugins/typeahead/typeahead.bundle.min.js',
                            'styles/assets/admin/pages/scripts/components-form-tools.js',

                            'styles/assets/admin/layout/scripts/quick-sidebar.js',
                            'styles/assets/admin/pages/scripts/table-editable.js',

                            'js/controllers/ProductController.js'
                        ] 
                    }]);
                }] 
            }
       
        })
        .state('agents', {
            url: "/agents",
            templateUrl: "views/agents.html",            
            data: {pageTitle: 'Admin|Agents'},
            controller: "AgentController",
              resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            'styles/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css',
                            'styles/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                            'styles/assets/global/plugins/typeahead/typeahead.css',


                            'styles/assets/global/plugins/select2/select2.css',
                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',

                            'styles/assets/global/css/components.css',
                            'styles/assets/global/css/plugins.css',
                            'styles/assets/admin/layout/css/layout.css',                                      
                            'styles/assets/admin/layout/css/themes/darkblue.css',
                            'styles/assets/admin/layout/css/custom.css',


                            

                            'styles/assets/global/plugins/fuelux/js/spinner.min.js',
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            'styles/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            'styles/assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
                            'styles/assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                            'styles/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js',
                            'styles/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                            'styles/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
                            'styles/assets/global/plugins/typeahead/handlebars.min.js',
                            'styles/assets/global/plugins/typeahead/typeahead.bundle.min.js',
                            'styles/assets/admin/pages/scripts/components-form-tools.js',

                            'styles/assets/admin/layout/scripts/quick-sidebar.js',
                            'styles/assets/admin/pages/scripts/table-editable.js',
                            'styles/assets/global/plugins/select2/select2.min.js',
                            'styles/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js',
                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js',
                            'styles/assets/admin/pages/scripts/table-editable.js',


                            'styles/assets/global/plugins/datatables/all.min.js',

                            'js/scripts/table-advanced.js',




                            'styles/assets/global/plugins/select2/select2.css',                             
                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css', 
                            'styles/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css',
                            'styles/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css',

                            'styles/assets/global/plugins/select2/select2.min.js',
                            'styles/assets/global/plugins/datatables/all.min.js',
                            'js/scripts/table-advanced.js',

                            'js/controllers/AgentController.js'
                        ] 
                    }]);
                }] 
            }
       
        })
        .state('reportagents', {
            url: "/reportagents",
            templateUrl: "views/report_agents.html",            
            data: {pageTitle: 'Admin|Agents Reports'},
            controller: "ReportAgentsController",
              resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            'styles/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css',
                            'styles/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                            'styles/assets/global/plugins/typeahead/typeahead.css',


                            'styles/assets/global/plugins/select2/select2.css',
                        

        

        

                            'styles/assets/admin/layout/scripts/quick-sidebar.js',
                            'styles/assets/admin/pages/scripts/table-editable.js',
                            'styles/assets/global/plugins/select2/select2.min.js',
                            'styles/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js',
                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js',
                            'styles/assets/admin/pages/scripts/table-editable.js',


                            'styles/assets/global/plugins/datatables/all.min.js',

                            'js/scripts/table-advanced.js',
                         
                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css', 
                            'styles/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css',
                            'styles/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css',

                            'styles/assets/global/plugins/select2/select2.min.js',
                            
                            'js/scripts/table-advanced.js',

                            'js/controllers/ReportAgentsController.js'
                        ] 
                    }]);
                }] 
            }
       
        })
        .state('report_sales', {
            url: "/reportsales",
            templateUrl: "views/report_sales.html",            
            data: {pageTitle: 'Admin| Products Reports'},
            controller: "ReportSalesController",
              resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            'styles/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css',
                            'styles/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                            'styles/assets/global/plugins/typeahead/typeahead.css',

                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',
                            'styles/assets/global/plugins/select2/select2.css',
                            'styles/assets/global/plugins/clockface/css/clockface.css',
                            'styles/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'styles/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',                          
                            'styles/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css',
                            'styles/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                            'styles/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',


                            'styles/assets/global/plugins/fuelux/js/spinner.min.js',
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            'styles/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            'styles/assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
                            'styles/assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                            'styles/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js',
                            'styles/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                            'styles/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
                            'styles/assets/global/plugins/typeahead/handlebars.min.js',
                            'styles/assets/global/plugins/typeahead/typeahead.bundle.min.js',
                            'styles/assets/admin/pages/scripts/components-form-tools.js',

                            'styles/assets/admin/layout/scripts/quick-sidebar.js',
                            'styles/assets/admin/pages/scripts/table-editable.js',
                            'styles/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                            'styles/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            'styles/assets/global/plugins/clockface/js/clockface.js',
                            'styles/assets/global/plugins/bootstrap-daterangepicker/moment.min.js',
                            'styles/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                            'styles/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
                            'styles/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',


                            "styles/assets/admin/pages/scripts/components-pickers.js",

                            'js/controllers/ReportSalesController.js'
                        ] 
                    }]);
                }] 
            }
       
        })
        .state('report_sales_most_sold_products', {
            url: "/reportsalesmostsoldproducts",
            templateUrl: "views/report_sales_most_sold_products.html",
            data: {pageTitle: 'Admin|Products Reports'},
            controller: "MostSoldProductsController",
              resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            'styles/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css',
                            'styles/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                            'styles/assets/global/plugins/typeahead/typeahead.css',

                            'styles/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',
                            'styles/assets/global/plugins/select2/select2.css',
                            'styles/assets/global/plugins/clockface/css/clockface.css',
                            'styles/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'styles/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',                          
                            'styles/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css',
                            'styles/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                            'styles/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',


                            'styles/assets/global/plugins/fuelux/js/spinner.min.js',
                            'styles/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            'styles/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            'styles/assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
                            'styles/assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                            'styles/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                            'styles/assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js',
                            'styles/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                            'styles/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
                            'styles/assets/global/plugins/typeahead/handlebars.min.js',
                            'styles/assets/global/plugins/typeahead/typeahead.bundle.min.js',
                            'styles/assets/admin/pages/scripts/components-form-tools.js',

                            'styles/assets/admin/layout/scripts/quick-sidebar.js',
                            'styles/assets/admin/pages/scripts/table-editable.js',
                            'styles/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                            'styles/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            'styles/assets/global/plugins/clockface/js/clockface.js',
                            'styles/assets/global/plugins/bootstrap-daterangepicker/moment.min.js',
                            'styles/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                            'styles/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
                            'styles/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',


                            "styles/assets/admin/pages/scripts/components-pickers.js",

                            'js/controllers/ReportSalesController.js'
                        ] 
                    }]);
                }] 
            }
       
        })


}]);

/* Init global settings and run the app */
MetronicApp.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
}]);
