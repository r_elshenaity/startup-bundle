'use strict';

MetronicApp.controller('AddlogController', function($rootScope, $scope, $http, $timeout, appSettings, $filter) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        Metronic.initAjax();
    });
   ///////////////////////////////////////Templating/////////////////////////////////////////////
    $scope.productsbox = [];
    $scope.customerIsVisible = false;
    $scope.productIsVisible = false;
    $scope.feedbacktype;
     var interests=[];
     var postres= -1;
     var log;
    $scope.addNewProduct = function() {
       
      var newItemNo = $scope.productsbox.length+1;
        $scope.productsbox.push({'id':'product'+newItemNo});
    };
    $scope.removeChoice = function() {
    var lastItem = $scope.productsbox.length-1;
    $scope.productsbox.splice(lastItem);
  };
  /////////////////////////////////////////////////////DB/////////////////////////////////////////
    
    $scope.clientForm = function () {
    $scope.client = {
      fullname: $scope.name,
      phone: $scope.phone,
      email: $scope.email,
      address: $scope.address,
      type:"customer"
        };
    
    $scope.postClient ($scope.client);
   
    
    };
     $scope.postClient = function (item) {
        // send post request
       $http.post(appSettings.db, item)
           .success(function (res) {
             $scope.status = 'Document Saved';
             console.log( $scope.status);
             
              $scope.doClick($scope.phone);
            
            }).error(function (res) {
             $scope.status = 'Error: ' + res.reason;
            console.log( $scope.status);
       });
    
     
      
    };
    $scope.postItem = function (item) {
        // send post request
       $http.post(appSettings.db, item)
           .success(function (res) {
             $scope.status = 'Document Saved';
             console.log( $scope.status);
            getid(res.id );
            
            }).error(function (res) {
             $scope.status = 'Error: ' + res.reason;
            console.log( $scope.status);
       });
    
     
      
    };
    function getid(id )
    {
        postres = id;
       if(postres.type != "log")
       {
         log={
                interests:interests,
                feedback:{feedback: $scope.feedback ,type: $scope.feedbacktype},
                invoice: postres,
                date: $filter('date')(new Date(), "dd/MM/yyyy"),
                customer:$scope.client.value._id,
                agent:"",
                type: "log"
            };
        }
        else
        {
           log={
                interests:interests,
                feedback:{feedback: $scope.feedback ,type: $scope.feedbacktype},
                date: $filter('date')(new Date(), "dd/MM/yyyy"),
                customer:$scope.client.value._id,
                agent:"",
                type: "log"
            };  
        }
             console.log( id);
            $http.post(appSettings.db, log)
           .success(function (data , res) {
             $scope.status = 'Document Saved';
             console.log( $scope.status);
            //getid(res.id , data);
            
            }).error(function (res) {
             $scope.status = 'Error: ' + res.reason;
            console.log( $scope.status);
       });
          // $scope.postItem (log);
       
       
    }
    function getItem (item) {
        // send post request
      $http.get(appSettings.db +'/'+ item)
          .success(function (data) {
            $scope.productdoc = data;
            $scope.productIsVisible = true;
           });
    };
    
    $scope.logForm = function () {
       var items = [];
       $scope.itemswithprice=[];
       $scope.total=0;
      
       var invoice;
      
       if($scope.productsbox.length > 0)
       {
           alert("invoice");
        for (var i=0; i < $scope.productsbox.length ; i++)
        {
        items[i] = {
               _id: $scope.productsbox[i].invoice_product,
               quantity: $scope.productsbox[i].invoice_quantity, 
               price : $scope.productsbox[i].invoice_quantity , 
           };
         }
        for (var jj=0; jj < items.length ; jj++)
        {
            for (var j=0; j < $scope.products.length ; j++)
             {
                 if(items[jj]._id == $scope.products[j].id )
                 {
                    $scope.itemswithprice[jj] = {
                         _id: $scope.products[j].id,
                         name: $scope.products[j].value.name,
                         quantity: items[jj].quantity, 
                         priceperitem: $scope.products[j].value.price,
                         price : $scope.products[j].value.price * items[jj].quantity , 
                     };
                     $scope.total+=$scope.products[j].value.price * items[jj].quantity;
                 }
             }
         }
       
            invoice={
            products: $scope.itemswithprice,
            date:$filter('date')(new Date(), "dd/MM/yyyy"),
            customer:$scope.client.value._id,
            status:"Pending",
            total:$scope.total,
            agent:"",
            type: "invoice"
            
           
       };
         
         $scope.postItem (invoice);
            
             
         
    }
    else
    {
        alert("log");
          log={
           interests:interests,
           feedback:{feedback: $scope.feedback ,type: $scope.feedbacktype},
           date: $filter('date')(new Date(), "dd/MM/yyyy"),
           customer:$scope.client.value._id,
           agent:"",
           type: "log"
       };
        getid(log);
       
    }
    
           
         
     
        // console.log( postres);
        
         
      };
    function getProducts () {
    $http.get(appSettings.db + '/_design/CouchApp/_view/byProduct')
      .success(function (data) {
        $scope.products = data.rows;
      
          
      });
      
    }
   
    getProducts();
    $scope.doClick = function(item, event) {
               
                var responsePromise =  $http.get(appSettings.db + '/_design/CouchApp/_view/customerbyPhone');
                responsePromise.success(function(data, status, headers, config) {
                   
                    for(var i =0; i < data.rows.length;i++)
                    {
                        if(data.rows[i].key[2] == $scope.phonecheck || data.rows[i].key[2] == item )
                        {
                            $scope.client = data.rows[i];
                            $scope.customerIsVisible = true;
                            if(item == null)
                            {
                            $scope.fromServer = "Available";
                            }
                            i=data.rows.length;
                            
                        }
                        else
                        {
                            if(item == null)
                            {
                            $scope.customerIsVisible = false;
                            }
                            $scope.fromServer = "Not Available"; 
                        }
                    }
                   
                });
                responsePromise.error(function(data, status, headers, config) {
                    $scope.fromServer = "server Out of Service";
                });
            };
    $scope.show_interest = function()
    {
        if($scope.interest != null)
        {
            
            $scope.interest=$scope.interest.split('+');
            alert(  $scope.interest);
             getItem($scope.interest[0]);
             
             interests.push($scope.interest);
        }
    };
     $scope.show_product = function(clicked)
    {
        getItem($scope.productsbox[clicked].invoice_product);
    };
     $scope.showcart = function()
    {
       var items = [];
       $scope.itemswithprice=[];
       $scope.total=0;
        if($scope.productsbox.length > 0)
       {
           
        for (var i=0; i < $scope.productsbox.length ; i++)
        {


         items[i] = {
               _id: $scope.productsbox[i].invoice_product,
               quantity: $scope.productsbox[i].invoice_quantity, 
               price : $scope.productsbox[i].invoice_quantity , 
           };
         }
        for (var jj=0; jj < items.length ; jj++)
        {
            for (var j=0; j < $scope.products.length ; j++)
             {
                 if(items[jj]._id == $scope.products[j].id )
                 {
                    $scope.itemswithprice[jj] = {
                         _id: $scope.products[j].id,
                         name: $scope.products[j].value.name,
                         quantity: items[jj].quantity, 
                         priceperitem: $scope.products[j].value.price,
                         price : $scope.products[j].value.price * items[jj].quantity , 
                     };
                     $scope.total+=$scope.products[j].value.price * items[jj].quantity;
                 }
             }
         }
       }
        
    };
         
    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
});

