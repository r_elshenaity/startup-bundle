'use strict';

MetronicApp.controller('CustomerController', function($rootScope, $scope, $http, $timeout, $stateParams, appSettings) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        Metronic.initAjax();
        console.log($stateParams.custId);

        $http.get(appSettings.db +'/'+ $stateParams.custId)
    .success(function (data) {
    	
      $scope.customer = data;
    
    });

    $http.get(appSettings.db +'/_design/CouchApp/_view/customerByLogs?include_docs=true&key="' + $stateParams.custId + '"')
    .success(function (data) {
       console.log(data); 
       
      $scope.logs = data.rows.map(function (o) { return o.value; });
    
    });

    });
   
    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = false;
});
