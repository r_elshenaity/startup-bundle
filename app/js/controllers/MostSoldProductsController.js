'use strict';

MetronicApp.controller('MostSoldProductsController', function($http, $rootScope, $scope, appSettings)  {

    $scope.date = null;
    $scope.total = null;
    $scope.invoice_number = null;
    $scope.items = [];
    $scope.status = '';

    $scope.updateItems = function(){
      var startkey = $scope.startkey;
      var endkey = $scope.endkey;
      console.log(appSettings.db + '/_design/CouchApp/_view/invoiceByDateRange' + '?startkey="' + startkey + '"&endkey="' + endkey + '"');
      $http.get(appSettings.db + '/_design/CouchApp/_view/invoiceByDateRange' + '?startkey="' + startkey + '"&endkey="' + endkey + '"')
        .success(function(data){
          var rows = data.rows;
          var row, newItem, itemdate, itemtotal, itemstatus, iteminvoice_number;
          for (var i =  0 ; i < rows.length ; i++) { //2
            $scope.items = [];
            row = rows[i];
            itemdate = row["value"]["date"];
            itemtotal = row["value"]["total"];
            iteminvoice_number = row["value"]["invoicenumber"];
            console.log(iteminvoice_number);
            itemstatus = row["value"]["status"];
            newItem = {
              date: itemdate,
              total: itemtotal,
              invoicenumber: iteminvoice_number,
              status : itemstatus
            };
            $scope.items.push(newItem);
          } //2
        });
    }

    function getItems () {
      $http.get(appSettings.db + '/_design/CouchApp/_view/invoiceByDateRange')
        .success(function (data) { // 3
          var rows = data.rows;
          var row, newItem, itemdate, itemtotal, itemstatus, iteminvoice_number;
          for (var i =  0 ; i < rows.length ; i++) { //2
            row = rows[i];
            itemdate = row["value"]["date"];
            itemtotal = row["value"]["total"];
            iteminvoice_number = row["value"]["invoicenumber"];
            console.log(iteminvoice_number);
            itemstatus = row["value"]["status"];
            newItem = {
              date: itemdate,
              total: itemtotal,
              invoicenumber: iteminvoice_number,
              status : itemstatus
            };
            $scope.items.push(newItem);
          } //2
          //$scope.items = data.rows;
        }); //3
    }
    getItems();
});

