'use strict';

MetronicApp.controller('ProductController', function($http, $rootScope, $scope, appSettings)  {

    $scope.name = null;
    $scope.price = null;
    $scope.specs = null;
    $scope.items = [];
    $scope.status = '';

    $scope.processForm = function () {
      console.log('specs:' + $scope.specs);
      var item = {
        name: $scope.name,
        price: $scope.price,
        specs: $scope.specs,
        type: 'product'
      };
      
      console.log(item.price);
      postItem(item);
    };

    $scope.deleteItem = function (item) {
      var id = item.id;

       $http.get(appSettings.db + '/' + item.id)
        .success(function(data){
          $http.delete(appSettings.db + '/' + data._id + '?rev=' + data._rev)
            .success(function(){
              //remove from scope?
              for (var i = 0; i < $scope.items.length; i++) {
                if($scope.items[i].id == id){
                  var element = $scope.items[i];
                  var index = $scope.items.indexOf(element);
                  $scope.items.splice(index, 1);
                  return;
                }
              };
            });
        });
    }

    function postItem (item) {
      
      // optimistic ui update
      $scope.items.push({name: $scope.name, price: $scope.price, specs: $scope.specs});
      // send post request
      $http.post(appSettings.db, item)
        .success(function () {

          $scope.status = '';
        }).error(function (res) {
          $scope.status = 'Error: ' + res.reason;
          // refetch items from server
          getItems();
        });
    }

    function getItems () {
      $http.get(appSettings.db + '/_design/CouchApp/_view/byProduct')
        .success(function (data) { // 3
          var rows = data.rows;
          var row, newItem, itemName, itemPrice, itemId, itemSpecs;
          for (var i =  0 ; i < rows.length ; i++) { //2
            row = rows[i];
            itemName = row["value"]["name"];
            itemPrice = row["value"]["price"];
            itemSpecs = row["value"]["specs"];
            itemId = row["value"]["_id"];
            // console.log(itemSpecs);
            // console.log(itemId);


            newItem = {
              name: itemName,
              price: itemPrice,
              specs: itemSpecs,
              id : itemId
            };
            $scope.items.push(newItem);
          } //2
          //$scope.items = data.rows;
        }); //3
    }
    getItems();
    
});

