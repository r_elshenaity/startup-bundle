'use strict';
MetronicApp.controller('ViewCustomersController', function($rootScope, $scope, $http, $timeout, $location, appSettings) {

$scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        Metronic.initAjax();
        $http.get(appSettings.db + '/_design/CouchApp/_view/allCustomers')
    .success(function (data) {
      $scope.customers = data.rows.map(function (o) { return o.value; });
    
    });
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = false;
    
   
});

