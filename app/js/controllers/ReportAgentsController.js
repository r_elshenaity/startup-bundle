'use strict';

MetronicApp.controller('ReportAgentsController', function($http, $rootScope, $scope,appSettings)  {
    
    $scope.items = [];

    function getItems () {
      $http.get(appSettings.db + '/_design/CouchApp/_view/agentsByCalls?group=true')
        .success(function(data){

          var rows = data.rows;
          var row, newItem;
          for(var i = 0; i< rows.length; i++){
            row = rows[i];
            newItem = {
              id: row.key,
              amount:  row.value
            };
            
            $scope.items.push(newItem);
          }
          //Loop to convert agent ID's to names
          for(var j = 0; j< $scope.items.length; j++) {
            $http.get(appSettings.db +"/"+ $scope.items[j].id)
              .success(function(data){
                for(var k = 0; k < $scope.items.length; k++) {
                  if($scope.items[k].id == data._id){
                    $scope.items[k].name = data.name;
                    return;
                  }
                }
              });
          }
        });
    }

    getItems();

});

