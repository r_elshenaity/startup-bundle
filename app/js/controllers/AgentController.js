'use strict';

MetronicApp.controller('AgentController', function($http, $rootScope, $scope, settings,appSettings)  {

	$scope.$on('$viewContentLoaded', function() {   
    	// initialize core components
    	Metronic.initAjax();

    	// set default layout mode
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });

    $scope.name = null;
    $scope.phone = null;
    $scope.items = [];
    $scope.status = '';

    $scope.processForm = function () {
      var item = {
        name: $scope.name,
        phone: $scope.phone,
        username: $scope.username,
        email : $scope.email,
        password: $scope.password,
        type: 'agent'
      };
      postItem(item);
    };

    $scope.deleteItem = function (item) {
      var id = item.id;

       $http.get(appSettings.db + '/' + item.id)
        .success(function(data){
          $http.delete(appSettings.db + '/' + data._id + '?rev=' + data._rev)
            .success(function(){
              //remove from scope?
              for (var i = 0; i < $scope.items.length; i++) {
                if($scope.items[i].id == id){
                  var element = $scope.items[i];
                  var index = $scope.items.indexOf(element);
                  $scope.items.splice(index, 1);
                  return;
                }
              };
            });
        });
    }

    function postItem (item) {
      
      // optimistic ui update
      $scope.items.push({name: $scope.name, phone: $scope.phone, email: $scope.email, username: $scope.username, password: $scope.password});
      // send post request
      $http.post(appSettings.db, item)
        .success(function () {

          $scope.status = '';
        }).error(function (res) {
          $scope.status = 'Error: ' + res.reason;
          // refetch items from server
          getItems();
        });
    }

    function getItems () {
      $http.get(appSettings.db + '/_design/CouchApp/_view/byAgent')
        .success(function (data) { // 3
          var rows = data.rows;
          console.log(rows);
          var row, newItem, itemName, itemPhone, itemId, itemUsername, itemEmail, itemPassword;
          for (var i =  0 ; i < rows.length ; i++) { //2
            row = rows[i];
            itemName = row["value"]["name"];
            itemPhone = row["value"]["phone"];
            itemEmail = row["value"]["email"];
            itemUsername = row["value"]["username"];
            itemPassword = row["value"]["password"];
            itemId = row["value"]["_id"];
            newItem = {
              name: itemName,
              phone: itemPhone,
              email: itemEmail,
              username: itemUsername,
              password: itemPassword,
              id : itemId
            };
            $scope.items.push(newItem);
          } //2
          //$scope.items = data.rows;
        }); //3
    }
    getItems();
    
});