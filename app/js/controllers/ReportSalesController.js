'use strict';

MetronicApp.controller('ReportSalesController', function($http, $rootScope, $scope,appSettings , $filter)  {
    
    $scope.date = null;
    $scope.total = null;
    $scope.invoice_number = null;
    $scope.items = [];
    $scope.products = [];
    $scope.status = '';
    $scope.updateItems = function(){
      var startkey = $scope.startkey;
      var endkey = $scope.endkey;
      console.log(appSettings.db + '/_design/CouchApp/_view/invoiceByDateRange' + '?startkey="' + startkey + '"&endkey="' + endkey + '"');
      $http.get(appSettings.db + '/_design/CouchApp/_view/invoiceByDateRange' + '?startkey="' + startkey + '"&endkey="' + endkey + '"')
        .success(function(data){
          $scope.items = [];
          var rows = data.rows;
          var row, newItem, itemdate, itemtotal, itemstatus, iteminvoice_number;
          for (var i =  0 ; i < rows.length ; i++) { //2
            row = rows[i];
            itemdate = row["value"]["date"];
            itemtotal = row["value"]["total"];
            iteminvoice_number = row["value"]["invoice_number"];
            console.log(iteminvoice_number);
            itemstatus = row["value"]["status"];
            newItem = {
              date: itemdate,
              total: itemtotal,
              invoice_number: iteminvoice_number,
              status : itemstatus
            };
            $scope.items.push(newItem);
          } //2
        });
    }

    function getItems () {
      var today = $filter('date')(new Date(), "dd/MM/yyyy");
//      var dd = today.getDate();
//      var mm = today.getMonth()+1;
//      var yyyy = today.getFullYear();
//      if(dd<10) {
//          dd='0'+dd
//      } 
//      if(mm<10) {
//          mm='0'+mm
//      } 
//      //today = '"'+dd+'-'+mm+'-'+yyyy+'"';
      console.log(appSettings.db + '/_design/CouchApp/_view/invoiceByDateRange' + '?key=' + '"'+ today +'"');
      $http.get(appSettings.db + '/_design/CouchApp/_view/invoiceByDateRange' + '?key='  + '"'+ today +'"')
        .success(function (data) { // 3
          var rows = data.rows;
          var row, newItem, itemdate, itemtotal, itemstatus, iteminvoice_number;
          for (var i =  0 ; i < rows.length ; i++) { //2
            row = rows[i];
            itemdate = row["value"]["date"];
            itemtotal = row["value"]["total"];
            iteminvoice_number = row["value"]["invoicenumber"];
            console.log(iteminvoice_number);
            itemstatus = row["value"]["status"];
            newItem = {
              date: itemdate,
              total: itemtotal,
              invoicenumber: iteminvoice_number,
              status : itemstatus
            };
            $scope.items.push(newItem);
          } //2
          //$scope.items = data.rows;
        }); //3
    }
    function getMostSoldProducts () {
      $http.get(appSettings.db + '/_design/CouchApp/_view/mostSoldProducts?group=true')
        .success(function (data) { // 3
          var rows = data.rows;
          console.log(rows);
          var row, newItem;
          for (var i =  0 ; i < rows.length ; i++) { //2
            row = rows[i];
            newItem = {
              id: row.key,
              quantity: row.value,
            };
            $scope.products.push(newItem);
          } //2
          for(var i = 0; i  < $scope.products.length; i++) {
            $http.get(appSettings.db + '/' + $scope.products[i].id)
              .success(function(data){
                for(var j = 0; j < $scope.products.length; j++) {
                  if($scope.products[j].id == data._id){
                    $scope.products[j].name = data.name;
                    return;
                  }
                }
              });
          }
          $scope.products.sort(function(a, b){
            var keyA = a.quantity,
            keyB = b.quantity;
            if(keyA < keyB) return 1;
            if(keyA > keyB) return -1;
            return 0;
          });
          //Top 3 items, to change -> change 3
          $scope.products = $scope.products.slice(0,3);
        }); //3
    }
    getItems();
    getMostSoldProducts();
});

