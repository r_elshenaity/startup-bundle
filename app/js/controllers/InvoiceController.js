'use strict';

MetronicApp.controller('InvoiceController', function($rootScope, $scope, $http, $timeout, $stateParams, appSettings, $filter) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        Metronic.initAjax();
$scope.invoices = [];
var invoices =[];

    $http.get(appSettings.db +'/_design/CouchApp/_view/allInvoices?include_docs=true')
      .success(function (data) {
         
    	var rows = data.rows;
        //console.log(data);
        for (var i = 0; i < rows.length;  i++) {
            var productsName = [];
            var invoice = rows[i].value.invoice;
            var customerName = rows[i].doc.fullname;
            var productId = rows[i].value.invoice.products;
            //console.log(customerName);
            $scope.invoices.push({
                id : invoice._id,
                rev : invoice._rev,
                invoiceNo : invoice.invoice_number,
                date : $filter('date')(invoice.date, "dd/MM/yyyy"),
                productId: productId,
                productName: productsName,
                total: invoice.total,
                customer: customerName,
                status: invoice.status,
                shippeddate: $filter('date')(invoice.shippeddate, "dd/MM/yyyy")
              });
            
            for(var l=0;l<productId.length;l++)
            {
                console.log(productId.length);
            $http.get(appSettings.db + '/' + productId[l]._id)
                .success(function(data){
                    
                    for(var j=0; j<$scope.invoices.length; j++) {
                        for (var k=0; k< $scope.invoices[j].productId.length; k++){
                            if($scope.invoices[j].productId[k]._id == data._id)
                            {
                                $scope.invoices[j].productName[k] = data.name;
                                //console.log($scope.invoices[j].productName[k]);
                                
                            }
                        }
                    }
                    
                });  
            }  
        };
          
 
      
    
    });


$scope.updateStatus=function(invoiceId, rev)
{
    var msg;
    $http.get(appSettings.db +'/' + invoiceId)
      .success(function (data) {
    data.status="Shipped";
msg=data;
var date=new Date();
msg.shippeddate= $filter('date')(date, "dd/MM/yyyy");
console.log(msg.shippeddate);
update(msg,invoiceId,rev);
});
}
function update(msg,invoiceId,rev){
    $http.put(appSettings.db + '/' + invoiceId + '?rev=' + rev, msg).
    success(function(data, status, headers, config) {
    
     for(var i=0; i< $scope.invoices.length; i++)
     {
        
        if($scope.invoices[i].id === data.id)
        {
            
            $scope.invoices[i].status="Shipped";
            $scope.invoices[i].shippeddate= msg.shippeddate;
        }
     }
    }).
    error(function(data, status, headers, config) {
      //TODO temporary, remove
      alert("ERROR " + status + ": " + data);
    });
}

    });
   
    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = false;
});
